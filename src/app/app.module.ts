import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {CommonModule} from '@angular/common';
import {ModeTabViewComponent} from './components/mode-tab-view/mode-tab-view.component';
import {
  ButtonModule, CardModule,
  CheckboxModule,
  ColorPickerModule,
  PanelModule,
  SliderModule,
  SpinnerModule,
  TabMenuModule,
  ToolbarModule
} from 'primeng/primeng';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StripesModeComponent} from './components/stripes-mode/stripes-mode.component';
import {BurnModeComponent} from './components/burn-mode/burn-mode.component';
import {StripesFormComponent} from './components/stripes-form/stripes-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BlinkModeComponent} from './components/blink-mode/blink-mode.component';
import {BlinkFormComponent} from './components/blink-form/blink-form.component';
import {BurnFormComponent} from './components/burn-form/burn-form.component';
import { PowerComponent } from './components/power/power.component';

@NgModule({
  declarations: [
    AppComponent,
    ModeTabViewComponent,
    StripesModeComponent,
    BurnModeComponent,
    StripesFormComponent,
    BlinkModeComponent,
    BlinkFormComponent,
    BurnFormComponent,
    PowerComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    TabMenuModule,
    SliderModule,
    ButtonModule,
    SpinnerModule,
    ColorPickerModule,
    PanelModule,
    ToolbarModule,
    CheckboxModule,
    CardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
