import {Component, OnInit} from '@angular/core';
import {ModesService} from './services/modes.service';
import {Modes} from './model/modes';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  private modes: Modes;

  constructor(private modesService: ModesService) {
  }

  ngOnInit(): void {
    this.modesService.$modes.subscribe(modes => this.modes = modes);
  }
}
