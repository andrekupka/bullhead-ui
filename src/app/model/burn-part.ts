import {Color} from './color';

export interface BurnPart {
  color: Color;
  secondarycolor: Color;
  burn: number;
  strobe: number;
}
