import {Color} from './color';

export declare type PartialBlink = Partial<Blink>;

export interface Blink {
  color: Color;
  ratio: number;
  speed: number;
}
