import {BurnPart} from './burn-part';

export interface Burn {
  [key: string]: BurnPart;
}

export function getBurnPartNames(burn: Burn): Array<string> {
  return Object.keys(burn);
}
