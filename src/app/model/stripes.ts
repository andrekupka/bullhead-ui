import {Color} from './color';

export declare type PartialStripes = Partial<Stripes>;

export interface Stripes {
  color: Color;
  length: number;
  gap: number;
  speed: number;
}
