export const BURN_MODE = 'burn';
export const STRIPES_MODE = 'stripes';
export const BLINK_MODE = 'blink';

export interface Modes {
  mode: string;
  availableModes: Array<string>;
}
