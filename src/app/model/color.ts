export interface Color {
  r: number;
  g: number;
  b: number;
}

export function hexToColor(hex: string): Color | null {
  // https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}

function toHex(n: number): string {
  const hex = Number(n).toString(16);
  if (hex.length < 2) {
    return '0' + hex;
  }
  return hex;
}

export function colorToHex(color: Color): string {
  return '#' + toHex(color.r) + toHex(color.g) + toHex(color.b);
}
