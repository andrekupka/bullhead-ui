import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Burn, getBurnPartNames} from '../../model/burn';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-burn-form',
  templateUrl: './burn-form.component.html',
})
export class BurnFormComponent implements OnInit, OnChanges {

  @Input() burn: Burn = null;

  @Input() autoUpdate = false;

  @Output() update = new EventEmitter<Burn>();

  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
    });

    this.burnPartNames.forEach(name => {
      const part = this.burn[name];
      this.form.addControl(name, this.formBuilder.group({
        burn: [part.burn, [
          Validators.required,
          Validators.min(0),
          Validators.max(255)
        ]],
        strobe: [part.strobe, [
          Validators.required,
          Validators.min(0),
          Validators.max(255)
        ]],
        color: [part.color, [
          Validators.required
        ]],
        secondarycolor: [part.secondarycolor, [
          Validators.required
        ]]
      }));
    });

    this.form.valueChanges.subscribe(value => {
      if (this.autoUpdate) {
        this.updateBurn(value);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    const autoUpdate = changes.autoUpdate;
    if (autoUpdate && !autoUpdate.isFirstChange() && autoUpdate.currentValue && autoUpdate.currentValue !== autoUpdate.previousValue) {
      this.updateBurn(this.form.value);
    }
  }

  private updateBurn(value: any): void {
    if (!this.isFormValid) {
      return;
    }

    const burn: Burn = {};
    const partNames = Object.keys(value);
    partNames.forEach(name => {
      const part = value[name];
      burn[name] = {
        burn: part.burn,
        strobe: part.strobe,
        color: part.color,
        secondarycolor: part.secondarycolor
      };
    });

    this.update.emit(burn);
  }

  public get burnPartNames(): Array<string> {
    return getBurnPartNames(this.burn);
  }

  public get isFormValid(): boolean {
    return !(this.form.invalid || this.form.pending);
  }

  public get canConfigure(): boolean {
    return this.isFormValid && !this.autoUpdate;
  }

  public submit(): void {
    if (this.canConfigure) {
      this.updateBurn(this.form.value);
    }
  }

}
