import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Stripes} from '../../model/stripes';

@Component({
  selector: 'app-stripes-form',
  templateUrl: './stripes-form.component.html',
})
export class StripesFormComponent implements OnInit, OnChanges {

  @Input() stripes: Stripes;

  @Input() autoUpdate = false;

  @Output() update = new EventEmitter<Stripes>();

  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      length: [this.stripes.length, [
        Validators.required,
        Validators.min(1),
        Validators.max(20)
      ]],
      gap: [this.stripes.gap, [
        Validators.required,
        Validators.min(1),
        Validators.max(20)
      ]],
      speed: [this.stripes.speed, [
        Validators.required,
        Validators.min(0),
        Validators.max(255)
      ]],
      color: [this.stripes.color, [
        Validators.required
      ]]
    });

    this.form.valueChanges.subscribe(value => {
      if (this.autoUpdate) {
        this.updateStripes(value);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    const autoUpdate = changes.autoUpdate;
    if (autoUpdate && !autoUpdate.isFirstChange() && autoUpdate.currentValue && autoUpdate.currentValue !== autoUpdate.previousValue) {
      this.updateStripes(this.form.value);
    }
  }

  updateStripes(value: any): void {
    if (!this.isFormValid) {
      return;
    }

    const stripes = {
      length: value.length,
      gap: value.gap,
      speed: value.speed,
      color: value.color
    };
    this.update.emit(stripes);
  }

  public get isFormValid(): boolean {
    return !(this.form.invalid || this.form.pending);
  }

  public get canConfigure(): boolean {
    return this.isFormValid && !this.autoUpdate;
  }

  public submit(): void {
    if (this.canConfigure) {
      this.updateStripes(this.form.value);
    }
  }
}
