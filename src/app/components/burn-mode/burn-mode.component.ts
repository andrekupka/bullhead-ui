import {Component, OnInit} from '@angular/core';
import {ModesService} from '../../services/modes.service';
import {BURN_MODE} from '../../model/modes';
import {HardwareService} from '../../services/hardware.service';
import {Hardware} from '../../model/hardware';
import {Burn} from '../../model/burn';
import {BurnService} from '../../services/burn.service';
import {Blink} from '../../model/blink';
import {BlinkService} from '../../services/blink.service';
import {AutoUpdateService} from '../../services/auto-update.service';

@Component({
  selector: 'app-burn-mode',
  templateUrl: './burn-mode.component.html',
})
export class BurnModeComponent implements OnInit {

  hardware: Hardware = null;

  burn: Burn = null;

  autoUpdate = false;

  constructor(private hardwareService: HardwareService, private burnService: BurnService, private autoUpdateService: AutoUpdateService) {
  }

  ngOnInit() {
    this.hardwareService.$hardware.subscribe(hardware => this.hardware = hardware);
    this.burnService.$burn.subscribe(burn => this.burn = burn);
    this.autoUpdateService.$autoUpdate.subscribe(autoUpdate => this.autoUpdate = autoUpdate)
  }


  public configureBurn(burn: Burn): void {
    this.burnService.configureBurn(burn).subscribe(() => {
    });
  }
}
