import {Component, OnInit} from '@angular/core';
import {Blink} from '../../model/blink';
import {BlinkService} from '../../services/blink.service';
import {AutoUpdateService} from '../../services/auto-update.service';

@Component({
  selector: 'app-blink-mode',
  templateUrl: './blink-mode.component.html',
})
export class BlinkModeComponent implements OnInit {

  blink: Blink = null;

  autoUpdate = false;

  constructor(private blinkService: BlinkService, private autoUpdateService: AutoUpdateService) {
  }

  ngOnInit() {
    this.blinkService.$blink.subscribe(blink => this.blink = blink);
    this.autoUpdateService.$autoUpdate.subscribe(autoUpdate => this.autoUpdate = autoUpdate);
  }

  public configureBlink(blink: Blink): void {
    this.blinkService.configure(blink).subscribe(() => {
    });
  }
}
