import {Component, OnInit} from '@angular/core';
import {Modes} from '../../model/modes';
import {ModesService} from '../../services/modes.service';
import {MenuItem} from 'primeng/api';
import {Router} from '@angular/router';
import {AutoUpdateService} from '../../services/auto-update.service';
import {PowerService} from '../../services/power.service';

@Component({
  selector: 'app-mode-tab-view',
  templateUrl: './mode-tab-view.component.html',
})
export class ModeTabViewComponent implements OnInit {

  modeItems: MenuItem[] = [];

  constructor(
    private modesService: ModesService,
    private autoUpdateService: AutoUpdateService,
    private powerService: PowerService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.modesService.$modes.subscribe(modes => this.setUpMenuItems(modes));
  }

  private setUpMenuItems(modes: Modes): void {
    const availableModes = modes.availableModes;
    const activeMode = modes.mode;

    this.modeItems = availableModes.map(mode => {
      const item = {
        label: mode,
        routerLink: ['/' + mode]
      };
      if (mode === activeMode) {
        item['icon'] = 'fa fa-fw fa-check';
      }
      return item;
    });

    this.router.navigate(['/' + activeMode]);
  }

  public activateSelectedMode(): void {
    this.modesService.activateSelectedMode().subscribe(() => {
    });
  }

  public setAutoUpdate(autoUpdate: boolean): void {
    this.autoUpdateService.setAutoUpdate(autoUpdate);
  }


}
