import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Blink} from '../../model/blink';

@Component({
  selector: 'app-blink-form',
  templateUrl: './blink-form.component.html',
})
export class BlinkFormComponent implements OnInit, OnChanges {

  @Input() blink: Blink;

  @Input() autoUpdate = false;

  @Output() update = new EventEmitter<Blink>();

  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      ratio: [this.blink.ratio, [
        Validators.required,
        Validators.min(0),
        Validators.max(255)
      ]],
      speed: [this.blink.speed, [
        Validators.required,
        Validators.min(0),
        Validators.max(255)
      ]],
      color: [this.blink.color, [
        Validators.required
      ]]
    });

    this.form.valueChanges.subscribe(value => {
      if (this.autoUpdate) {
        this.updateBlink(value);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    const autoUpdate = changes.autoUpdate;
    if (autoUpdate && !autoUpdate.isFirstChange() && autoUpdate.currentValue && autoUpdate.currentValue !== autoUpdate.previousValue) {
      this.updateBlink(this.form.value);
    }
  }

  private updateBlink(value: any): void {
    if (!this.isFormValid) {
      return;
    }

    const blink = {
      ratio: value.ratio,
      speed: value.speed,
      color: value.color
    };
    this.update.emit(blink);
  }

  public get isFormValid(): boolean {
    return !(this.form.invalid || this.form.pending);
  }

  public get canConfigure(): boolean {
    return this.isFormValid && !this.autoUpdate;
  }

  public submit(): void {
    if (this.canConfigure) {
      this.updateBlink(this.form.value);
    }
  }
}
