import {Component, OnInit} from '@angular/core';
import {PowerService} from '../../services/power.service';

@Component({
  selector: 'app-power',
  templateUrl: './power.component.html',
})
export class PowerComponent implements OnInit {

  private hasPower = false;

  constructor(private powerService: PowerService) { }

  ngOnInit(): void {
    this.powerService.$power.subscribe(power => this.hasPower = power.power);
  }

  public get power(): boolean {
    return this.hasPower;
  }

  public setPower(power: boolean): void {
    this.powerService.setPower({
      power: power
    }).subscribe(() => {});
  }

  public shutdown(): void {
    this.powerService.shutdown().subscribe(() => {});
  }
}
