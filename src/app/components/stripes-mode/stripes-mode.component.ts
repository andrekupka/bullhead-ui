import {Component, OnInit} from '@angular/core';
import {Stripes} from '../../model/stripes';
import {StripesService} from '../../services/stripes.service';
import {ModesService} from '../../services/modes.service';
import {STRIPES_MODE} from '../../model/modes';
import {AutoUpdateService} from '../../services/auto-update.service';

@Component({
  selector: 'app-stripes-mode',
  templateUrl: './stripes-mode.component.html',
})
export class StripesModeComponent implements OnInit {

  stripes: Stripes;

  autoUpdate: boolean;

  constructor(private stripesService: StripesService, private autoUpdateService: AutoUpdateService) {
  }

  ngOnInit() {
    this.stripesService.$stripes.subscribe(stripes => this.stripes = stripes);
    this.autoUpdateService.$autoUpdate.subscribe(autoUpdate => this.autoUpdate = autoUpdate);
  }

  public configureStripes(stripes: Stripes): void {
    this.stripesService.configure(stripes).subscribe(() => {
    });
  }
}
