import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ModesResolver} from './resolvers/modes.resolver';
import {ModeTabViewComponent} from './components/mode-tab-view/mode-tab-view.component';
import {StripesModeComponent} from './components/stripes-mode/stripes-mode.component';
import {BurnModeComponent} from './components/burn-mode/burn-mode.component';
import {HardwareResolver} from './resolvers/hardware.resolver';
import {BurnResolver} from './resolvers/burn.resolver';
import {StripesResolver} from './resolvers/stripes.resolver';
import {BLINK_MODE, BURN_MODE, STRIPES_MODE} from './model/modes';
import {SelectedModeResolver} from './resolvers/selected-mode.resolver';
import {BlinkModeComponent} from './components/blink-mode/blink-mode.component';
import {BlinkResolver} from './resolvers/blink.resolver';
import {PowerResolver} from './resolvers/power.resolver';

const ROUTES: Routes = [
  {
    path: '',
    component: ModeTabViewComponent,
    resolve: {
      modes: ModesResolver,
      hardware: HardwareResolver,
      power: PowerResolver,
    },
    children: [
      {
        path: 'burn',
        component: BurnModeComponent,
        data: {
          mode: BURN_MODE
        },
        resolve: {
          selectedMode: SelectedModeResolver,
          burn: BurnResolver
        }
      },
      {
        path: 'stripes',
        component: StripesModeComponent,
        data: {
          mode: STRIPES_MODE
        },
        resolve: {
          selectedMode: SelectedModeResolver,
          stripes: StripesResolver
        }
      },
      {
        path: 'blink',
        component: BlinkModeComponent,
        data: {
          mode: BLINK_MODE
        },
        resolve: {
          selectedMode: SelectedModeResolver,
          blink: BlinkResolver,
        }
      }
    ]
  },
  {
    path: '**',
    pathMatch:  'full',
    redirectTo: '/'
  }
];


@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
