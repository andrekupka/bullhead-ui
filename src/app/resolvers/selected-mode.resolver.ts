import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ModesService} from '../services/modes.service';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SelectedModeResolver implements Resolve<string> {

  constructor(private modesService: ModesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string {
    const selectedMode = route.data.mode;
    this.modesService.selectMode(selectedMode);
    return selectedMode;
  }
}
