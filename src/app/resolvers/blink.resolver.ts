import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Blink} from '../model/blink';
import {BlinkService} from '../services/blink.service';

@Injectable({
  providedIn: 'root'
})
export class BlinkResolver implements Resolve<Blink> {

  constructor(private blinkService: BlinkService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Blink> {
    return this.blinkService.loadBlink();
  }
}
