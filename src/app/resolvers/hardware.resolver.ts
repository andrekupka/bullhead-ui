import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Modes} from '../model/modes';
import {Observable} from 'rxjs';
import {ModesService} from '../services/modes.service';
import {Hardware} from '../model/hardware';
import {HardwareService} from '../services/hardware.service';

@Injectable({
  providedIn: 'root'
})
export class HardwareResolver implements Resolve<Hardware> {

  constructor(private hardwareService: HardwareService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Hardware> {
    return this.hardwareService.loadHardware();
  }
}
