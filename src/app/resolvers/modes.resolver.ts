import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Modes} from '../model/modes';
import {Observable} from 'rxjs';
import {ModesService} from '../services/modes.service';

@Injectable({
  providedIn: 'root'
})
export class ModesResolver implements Resolve<Modes> {

  constructor(private modesService: ModesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Modes> {
    return this.modesService.loadModes();
  }
}
