import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Modes} from '../model/modes';
import {Observable} from 'rxjs';
import {ModesService} from '../services/modes.service';
import {Stripes} from '../model/stripes';
import {StripesService} from '../services/stripes.service';

@Injectable({
  providedIn: 'root'
})
export class StripesResolver implements Resolve<Stripes> {

  constructor(private stripesService: StripesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Stripes> {
    return this.stripesService.loadStripes();
  }
}
