import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Modes} from '../model/modes';
import {Observable} from 'rxjs';
import {ModesService} from '../services/modes.service';
import {Stripes} from '../model/stripes';
import {StripesService} from '../services/stripes.service';
import {Burn} from '../model/burn';
import {BurnService} from '../services/burn.service';

@Injectable({
  providedIn: 'root'
})
export class BurnResolver implements Resolve<Burn> {

  constructor(private burnService: BurnService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Burn> {
    return this.burnService.loadBurn();;
  }
}
