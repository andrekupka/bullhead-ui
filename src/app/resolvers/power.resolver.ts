import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Blink} from '../model/blink';
import {BlinkService} from '../services/blink.service';
import {Power} from '../model/power';
import {PowerService} from '../services/power.service';

@Injectable({
  providedIn: 'root'
})
export class PowerResolver implements Resolve<Power> {

  constructor(private powerService: PowerService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Power> {
    return this.powerService.loadPower();
  }
}
