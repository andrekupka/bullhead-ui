import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {Hardware} from '../model/hardware';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HardwareService {

  private hardwareSubject = new BehaviorSubject({
    led: []
  });

  constructor(private apiService: ApiService) {
  }

  public get $hardware(): Observable<Hardware> {
    return this.hardwareSubject.asObservable();
  }

  public loadHardware(): Observable<Hardware> {
    return this.apiService.getHardware().pipe(
      tap(hardware => this.hardwareSubject.next(hardware))
    );
  }
}
