import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutoUpdateService {

  private autoUpdateSubject = new BehaviorSubject<boolean>(false);

  public get $autoUpdate(): Observable<boolean> {
    return this.autoUpdateSubject.asObservable();
  }

  public setAutoUpdate(autoUpdate: boolean): void {
    this.autoUpdateSubject.next(autoUpdate);
  }
}
