import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Burn} from '../model/burn';
import {ApiService} from './api.service';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BurnService {

  private burnSubject = new BehaviorSubject<Burn>({});

  constructor(private apiService: ApiService) {
  }

  public get $burn(): Observable<Burn> {
    return this.burnSubject.asObservable();
  }

  public loadBurn(): Observable<Burn> {
    return this.apiService.getBurn().pipe(
      tap(burn => this.updateBurn(burn))
    );
  }

  public configureBurn(burn: Burn): Observable<void> {
    return this.apiService.configureBurn(burn).pipe(
      tap(() => this.updateBurn(burn))
    );
  }

  private updateBurn(burn: Burn): void {
    this.burnSubject.next(burn);
  }
}
