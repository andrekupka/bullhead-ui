import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Power} from '../model/power';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PowerService {

  private powerSubject = new BehaviorSubject<Power>({
    power: false
  });

  constructor(private apiService: ApiService) {
  }

  public get $power(): Observable<Power> {
    return this.powerSubject.asObservable();
  }

  public loadPower(): Observable<Power> {
    const power = {
      power: true
    };
    this.updatePower(power);
    return of(power);
    //return this.apiService.getPower().pipe(
    //  tap(power => this.updatePower(power))
    //);
  }

  public setPower(power: Power): Observable<Power> {
    return this.apiService.updatePower(power).pipe(
      tap(newPower => this.updatePower(newPower))
    );
  }

  private updatePower(power: Power): void {
    this.powerSubject.next(power);
  }

  public shutdown(): Observable<void> {
    return this.apiService.shutdown();
  }
}
