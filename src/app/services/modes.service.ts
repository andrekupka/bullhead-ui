import {BehaviorSubject, Observable} from 'rxjs';
import {Modes} from '../model/modes';
import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';
import {ApiService} from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ModesService {

  private modes: Modes;

  private selectedMode: string;

  private modesSubject = new BehaviorSubject<Modes>(null);

  constructor(private apiService: ApiService) {
  }

  public get $modes(): Observable<Modes> {
    return this.modesSubject.asObservable();
  }

  public selectMode(mode: string): void {
    this.selectedMode = mode;
  }

  public activateSelectedMode(): Observable<void> {
    return this.activateMode(this.selectedMode);
  }

  public activateMode(mode: string): Observable<void> {
    const newModes = Object.assign({}, this.modes, {mode: mode});
    return this.apiService.activateMode(mode).pipe(
      tap(() => this.updateModes(newModes))
    );
  }

  public loadModes(): Observable<Modes> {
    return this.apiService.getModes().pipe(
      tap(modes => this.updateModes(modes))
    );
  }

  private updateModes(modes: Modes) {
    this.modes = modes;
    this.modesSubject.next(modes);
  }
}
