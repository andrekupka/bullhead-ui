import {Modes} from '../model/modes';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {PartialStripes, Stripes} from '../model/stripes';
import {Hardware} from '../model/hardware';
import {Burn} from '../model/burn';
import {Blink, PartialBlink} from '../model/blink';
import {Power} from '../model/power';

const BASE_URL = '/api';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  public getHardware(): Observable<Hardware> {
    return this.http.get<Hardware>(`${BASE_URL}/hardware`);
  }

  public getModes(): Observable<Modes> {
    return this.http.get<Modes>(`${BASE_URL}/modes`);
  }

  public activateMode(mode: string): Observable<void> {
    return this.http.post<void>(`${BASE_URL}/modes`, {
      mode: mode
    });
  }

  public getBurn(): Observable<Burn> {
    return this.http.get<Burn>(`${BASE_URL}/modes/burn`);
  }

  public configureBurn(burn: Burn): Observable<void> {
    return this.http.post<void>(`${BASE_URL}/modes/burn`, burn);
  }

  public getStripes(): Observable<Stripes> {
    return this.http.get<Stripes>(`${BASE_URL}/modes/stripes`);
  }

  public configureStripes(stripes: PartialStripes): Observable<void> {
    return this.http.post<void>(`${BASE_URL}/modes/stripes`, stripes);
  }

  public getBlink(): Observable<Blink> {
    return this.http.get<Blink>(`${BASE_URL}/modes/blink`);
  }

  public configureBlink(blink: PartialBlink): Observable<void> {
    return this.http.post<void>(`${BASE_URL}/modes/blink`, blink);
  }

  public getPower(): Observable<Power> {
    return this.http.get<Power>(`${BASE_URL}/power`);
  }

  public updatePower(power: Power): Observable<Power> {
    return this.http.post<Power>(`${BASE_URL}/power`, power);
  }

  public shutdown(): Observable<void> {
    return this.http.post<void>(`${BASE_URL}/shutdown`, {});
  }
}
