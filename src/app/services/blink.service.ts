import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ApiService} from './api.service';
import {tap} from 'rxjs/operators';
import {Blink} from '../model/blink';

@Injectable({
  providedIn: 'root'
})
export class BlinkService {

  private blinkSubject = new BehaviorSubject<Blink>({
    color: {
      r: 255,
      g: 0,
      b: 0
    },
    ratio: 128,
    speed: 128
  });

  constructor(private apiService: ApiService) {
  }

  public get $blink(): Observable<Blink> {
    return this.blinkSubject.asObservable();
  }

  public loadBlink(): Observable<Blink> {
    return this.apiService.getBlink().pipe(
      tap(blink => this.updateBlink(blink))
    );
  }

  public configure(blink: Blink): Observable<void> {
    return this.apiService.configureBlink(blink).pipe(
      tap(() => this.updateBlink(blink))
    );
  }

  private updateBlink(blink: Blink): void {
    this.blinkSubject.next(blink);
  }
}
