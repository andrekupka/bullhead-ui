import {Injectable} from '@angular/core';
import {Stripes} from '../model/stripes';
import {BehaviorSubject, Observable} from 'rxjs';
import {ApiService} from './api.service';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StripesService {

  private stripesSubject = new BehaviorSubject<Stripes>({
    length: 1,
    gap: 1,
    speed: 128,
    color: {
      r: 255,
      g: 0,
      b: 0
    }
  });

  constructor(private apiService: ApiService) {
  }

  public get $stripes(): Observable<Stripes> {
    return this.stripesSubject.asObservable();
  }

  public loadStripes(): Observable<Stripes> {
    return this.apiService.getStripes().pipe(
      tap(stripes => this.updateStripes(stripes))
    );
  }

  public configure(stripes: Stripes): Observable<void> {
    return this.apiService.configureStripes(stripes).pipe(
      tap(() => this.updateStripes(stripes))
    );
  }

  private updateStripes(stripes: Stripes): void {
    this.stripesSubject.next(stripes);
  }
}
